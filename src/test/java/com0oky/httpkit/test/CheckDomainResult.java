/**
 * @author mdc
 * @date 2017年7月5日 下午4:40:46
 */
package com0oky.httpkit.test;

/**
 * @author mdc
 * @date 2017年7月5日 下午4:40:46
 */
public class CheckDomainResult {
	
	private Result result;

	/**
	 * @return 获取{@link #result}
	 */
	public Result getResult() {
		return result;
	}

	/**
	 * @param result 设置result
	 */
	public void setResult(Result result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "CheckDomainResult [result=" + result + "]";
	}
}
